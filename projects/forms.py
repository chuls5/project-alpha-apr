from django import forms
from projects.models import Project


# Create New Project Form
class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )
